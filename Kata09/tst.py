import json

class CheckOut:

    lista = {}
    data = {}

    def new(self, pricing_rules):
        with open(pricing_rules) as data_file:
            self.data = json.load(data_file)

    def scan(self, item):
        if item in self.lista:
            self.lista[item]+=1
        else:
            self.lista[item] = 1

    def total(self):
        total = 0
        for item in self.lista:
            if item in self.data:
                if self.data[item]["special_price"] != [] and self.lista[item] >= self.data[item]["special_price"][0]:
                    total += ( int(self.lista[item]/self.data[item]["special_price"][0]) * self.data[item]["special_price"][1] ) + ( (self.lista[item]%self.data[item]["special_price"][0]) * self.data[item]["unit_price"] )
                else:
                    total += self.lista[item] * self.data[item]["unit_price"]
        return total

co = CheckOut()
co.new("pricing_rules.json")
co.scan("A")
co.scan("A")
co.scan("A")

print(co.total())

from pages.aboutpage import ChatPage
from hamcrest import assert_that, equal_to, contains_string
from util import *

@given(u'I am on Chat Tab')
def step_impl(context):
    context.page_object = ChatPage(context.driver)

@when(u'I click on new chat button')
def step_impl(context):
    context.page_object.click_on_new_message_button()

@then(u'I must see the Contact List')
def step_impl(context):
    assert_that(context.page_object.get_text_screen(), contains_string(('Select contact')))

@then(u'I click on Search button')
def step_impl(context):
    context.page_object.click_on_search_button()

@then(u'I insert "{name}"')
def step_impl(context, name):
    context.page_object.search_friend_name(name)

@thenu(u'I check the friend name')
def step_impl(context):
    assert_that(context.page_object.get_friend_name(), equal_to('Deyvirson')

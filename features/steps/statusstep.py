from pages.aboutpage import StatusPage
from hamcrest import assert_that, equal_to, contains_string
from util import *

@given(u'I am on Status Tab')
def step_impl(context):
    context.page_object = StatusPage(context.driver)

@when(u'I click on add status button')
def step_impl(context):
    context.page_object.click_on_add_status_button()

@then(u'I click to take a photo')
def step_impl(context):
    context.page.object.click_on_shutter_button()

@then(u'I send the photo to status')
def step_impl(context):
    context.page_object.add_image_status()

from pages.basepage import BasePage
from selenium.webdriver.common.by import By
from util import *

class StatusPage(BasePage):
    add_status_button = (By.ID, "fab")
    shutter_button = (By.ID, "shutter")
    send_image_button = (By.ID, "send")
    option_button = (By.ID, "action")

    def click_on_add_status_button(self):
        super().click(self.add_status_button)

    def click_on_shutter_button(self):
        super().click(self.shutter_button)

    def add_image_status(self):
        super().click(self.send_image_button)

    def click_on_option_button(self):
        super().click(self.option_button)

from pages.basepage import BasePage
from selenium.webdriver.common.by import By
from util import *

class ChatPage(BasePage):
    new_message_button = (By.ID, "fab")
    status_page = (By.XPATH, "//android.widget.TextView[@text='STATUS']")
    select_contact_text = "android.widget.TextView"
    search_button = (By.ID, "menuitem_search")
    search_input = (By.ID, "search_src_text")
    friend_name = (By.XPATH, "//android.widget.TextView[@text='Deyvirson']")

    def click_on_new_message_button(self):
        super().click(self.new_message_button)

    def get_text_screen(self):
        return super().elements_by_class(self.select_contact_text).text

    def click_on_search_button(self):
        return super().click(self.search_button)

    def search_friend_name(self, name):
        super().type_in(self.search_input,name)

    def get_friend_name(self):
            return super().find(self.friend_name).text

Feature: status

Background:
Given I am on Chat Tab

Scenario: Add status image
    Then I click on status tab
    Given I am on status list screen
    Then I click on add status button
    And I take a photo
    And I send the photo to status
    Then I see the photo on status

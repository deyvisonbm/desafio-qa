from appium import webdriver

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['deviceName'] = 'Galaxy S5'

def before_feature(context, feature):
    context.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
